class GamesController < ApplicationController

  def generate_word
    words = File.read('app/assets/words.txt').lines
    return words.select {|l| (4..20).cover?(l.strip.size)}.sample.strip
  end
  
  def new
    solution = generate_word
    progress = Array.new(solution.length,"*").join()
    game = Game.new(solution:solution,progress:progress,guess_count:10)
    game.save
    redirect_to :action => 'play' , :id =>game.id
    
  end
  
  def play
    @game = Game.find(params[:id])
  end
  
  def update
    game = Game.find(params[:id])
    
    if game.guess_count > 0
      
      
      guess = params[:guess]
      progress = game.progress
      if game.solution.include? guess
        game.solution.chars.each_with_index do |letter,i|
          if letter == guess
            progress[i] = letter
    				
          end
        end
        game.update_attributes(progress: progress)    
      else
      	game.update_attributes(guess_count: game.guess_count-1)	
      end     

      
    end
    redirect_to :action => 'play' , :id =>game.id
  end  
  
  def stats
  	@games = Game.all
  	@wins = Game.where("guess_count >= 0 AND solution = progress").count 
		@losses = Game.where("guess_count <= 0").count
		

  end  
  

end
