class Game < ApplicationRecord

  def generate_word
    words = File.read('app/assets/words.txt').lines
    return words.select {|l| (4..20).cover?(l.strip.size)}.sample.strip
  end

end
