module GamesHelper
  
  
  def progressForDisplay(progressStr)
    displayStr = Array.new(progressStr.length)
    progressStr.chars.each_with_index do |letter,i|
    	if letter == "*"
    		displayStr[i] = "  __  "
    	else
    		displayStr[i] = letter
    	end	
    end
    return displayStr.join()
    
  end
  
  def isOver(game)
  	return won(game) || lost(game)
  end
  
  def lost(game)
  	return game.guess_count < 1 && (game.solution != game.progress)
  end
  
  def won(game)
  	return (game.solution == game.progress)
  end
  
  def showHangman(num)
  	if num < 0
  		num = 0
  	end
  	return image_tag("hangmen/"+num.to_s+".png")
  end
end
