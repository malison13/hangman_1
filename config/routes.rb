Rails.application.routes.draw do
  root 'static_pages#home'
  get  '/new',    to: 'games#new'
  get  '/game',    to: 'games#play'
  get  '/stats',    to: 'games#stats'
  post '/update', to: 'games#update'

  get  '/about',   to: 'static_pages#about'
  resources :games
end
