class AddIndexToGamesGuessCount < ActiveRecord::Migration[5.1]
  def change
  	add_index :games, :guess_count, unique: false
  end
end
