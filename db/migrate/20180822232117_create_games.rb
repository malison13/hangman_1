class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.string :solution
      t.integer :guess_count
      t.string :progress

      t.timestamps
    end
  end
end
